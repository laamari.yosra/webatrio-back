const app = require('rcs-cardigan').app;
const static = require('rcs-cardigan').static;
app.use('/', static(__dirname + '/app'));
const port = process.env.PORT || 3000;

const options = {
    port: port,
    cors: true,
    databaseUrl: 'mongodb://yosra:yosra@ds131742.mlab.com:31742/webatrio'
}

app.start(options);

const users = require('./models/users.model.js');
const clients = require('./models/clients.model.js');
const companies = require('./models/companies.model.js');
const providers = require('./models/providers.model.js');
const projects = require('./models/projects.model.js');
const events = require('./models/events.model.js');
const requests = require('./models/requests.model.js');

app.authModel(users, { emailValdation: false });
app.addCollection(clients);
app.addCollection(companies);
app.addCollection(providers);
app.addCollection(projects);
app.addCollection(events);
app.addCollection(requests);

